﻿var utils = require('./utils');
var _ = require('lodash');

// The maxium allowed difference between speed and targetspeed
var SPEED_THRESHOLD = 0.1;
var LOGGING = true;
// ThrottleControl requires the pieces to form its
// own model of the race track
var ThrottleControl = function (game, speedPrediction, pieces) {

    // Track model
    this._game = game;
    this._pieces = [];
    this._deacceleration = undefined;
    this._previousThrottle = 0;
    this._speedPrection = speedPrediction;
    this._previousPiecePosition = null;
    this._piecesSinceCrash = Infinity;
    // Iterate through the track and add all the
    // target speeds

    LOGGING ? console.log("--Track Data") : null;

    for (var i = 0; i < pieces.length; ++i)
    {
        var trackPiece =
            {
                pieceIndex: i,
                targetSpeed: speedPrediction.getTargetSpeed(i),
                length: pieces[i].length,
                isBend: utils.isBend(pieces[i]),
                breakingDistance: utils.isBend(pieces[i]) ? 90 : 0,
                breakingDistanceChanged: false
            };

        LOGGING ? console.log("Index:", i) : null;
        LOGGING ? console.log("Target speed:", trackPiece.targetSpeed) : null;

        this._pieces.push(trackPiece);
    }
};

//Improved Throttle Control
ThrottleControl.prototype.getThrottle = function (piecePosition, speed, acceleration, throttle) {
    var speedChanged = false;

    for (var i = 0; i < this._pieces.length; ++i) {
        if (i === piecePosition.pieceIndex) {
            var targetSpeedDiff = Math.abs(this._pieces[i].targetSpeed - this._speedPrection.getTargetSpeed(i));

            if (targetSpeedDiff != NaN && targetSpeedDiff > 0.1) {
                speedChanged = true;
            }
        }

        this._pieces[i].targetSpeed = this._speedPrection.getTargetSpeed(i);
    }
    console.log('speedChanged', speedChanged)
    // Indexes for pieces
    var currentIndex = piecePosition.pieceIndex;
    var nextIndex = (currentIndex + 1) % this._pieces.length;
    var previousIndex = currentIndex - 1 < 0 ? this._pieces.length - 1 : currentIndex - 1;
    // The actual pieces]
    var currentPiece = this._pieces[currentIndex];
    var nextPiece = this._pieces[nextIndex];
    var previousPiece = this._pieces[previousIndex];

    var pieceChanged = false;

    // Check if the piece has changed between ticks
    if (this._previousPiecePosition != null
        && this._previousPiecePosition.pieceIndex != currentIndex)
    {
        pieceChanged = true;
    }
    console.log('pieceChanged', pieceChanged)

    var targetSpeed = nextPiece.targetSpeed;
    var distanceToPiece = Math.min(currentPiece.length - piecePosition.inPieceDistance, currentPiece.length);

    if (nextPiece.breakingDistance < distanceToPiece)
    {
        targetSpeed = currentPiece.targetSpeed;
    }

    if (currentPiece.isBend && !nextPiece.isBend) {
        targetSpeed = currentPiece.targetSpeed;
    }

    if (currentPiece.isBend && nextPiece.isBend
        && currentPiece.targetSpeed > nextPiece.targetSpeed) {
        targetSpeed = nextPiece.targetSpeed;
    }

    var ratio = targetSpeed / speed;
    var newThrottle = 1;

    if (ratio != Infinity) {

        if (ratio < 1.0)
        {
            newThrottle = 0.0;
        }
        else
        {
            newThrottle = 1.0;
        }
    }
    else
    {
        newThrottle = 1;
    }


    // If we are in a bend and need to do quick corrections
    if (currentPiece.targetSpeed < nextPiece.targetSpeed) {
        targetSpeed = currentPiece.targetSpeed;
        if (speed < targetSpeed)
        {
            newThrottle = 1;
        }
        else
        {
            newThrottle = 0;
        }

    }

    if (newThrottle == 0 && speed < 1)
    {
        newThrottle = 1;
    }

    /*
    LOGGING ? console.log("Current speed:", speed) : null;
    LOGGING ? console.log('Target speed:', nextPiece.targetSpeed) : null;
    LOGGING ? console.log("Ratio:", ratio) : null;
    LOGGING ? console.log('New Throttle:', newThrottle) : null;
    LOGGING ? console.log('Piece Index:', currentIndex) : null;
    LOGGING ? console.log('InPieceDistance:', piecePosition.inPieceDistance) : null;
    */

    // If piece had changed then we check if we achieved the targetspeed
    if (pieceChanged)
    {
        this._piecesSinceCrash++;

        if ((speed - currentPiece.targetSpeed) > 0.5)
        {
            // if the speed is higher then it should be
            // we adjust the breakingDistance accordingly
            var previousDistance = currentPiece.breakingDistance;
            currentPiece.breakingDistance *= (1 / ratio);


            LOGGING ? console.log("\n\n--Breaking distance changed to larger") : null;
            LOGGING ? console.log("Previous distance:", previousDistance) : null;
            LOGGING ? console.log("New distance:", currentPiece.breakingDistance) : null;
            LOGGING ? console.log("Target speed:", targetSpeed) : null;
            LOGGING ? console.log("Pieces target speed:", currentPiece.targetSpeed) : null;
            LOGGING ? console.log("Current speed:", speed) : null;
            LOGGING ? console.log("\n\n") : null;
        }

        // Set the previous piece distance changed to false
        // for the next round
        previousPiece.breakingDistanceChanged = false;
    }
    else
    {
        // If we have just crashed then dont do positive changes
        if (this._piecesSinceCrash > 3)
        {
            if (currentPiece.isBend != true  || nextPiece.isBend != true)
            {
                // If we hit the target speed before the piece then
                // we adjust the breaking distance accordingly
                if (Math.abs(targetSpeed - speed) < 0.5 && nextPiece.breakingDistanceChanged == false)
                {
                    nextPiece.breakingDistance -= (distanceToPiece / 2);
                    LOGGING ? console.log("\n\n--Breaking distance changed to smaller") : null;
                    LOGGING ? console.log("Previous distance:", previousDistance) : null;
                    LOGGING ? console.log("New distance:", nextPiece.breakingDistance) : null;
                    LOGGING ? console.log("Target speed:", targetSpeed) : null;
                    LOGGING ? console.log("Pieces target speed:", nextPiece.targetSpeed) : null;
                    LOGGING ? console.log("Current speed:", speed) : null;
                    LOGGING ? console.log("\n\n") : null;
                    nextPiece.breakingDistanceChanged = true;
                }
            }
            }
    }
    if (speed > 0.0001 && acceleration > 0.00001)
    {
        this._piecesSinceCrash = 0;
    }
    this._previousPiecePosition = piecePosition;

    var piecesInside = utils.piecesInsideLength(this._game.race.track.pieces, 100, currentIndex, piecePosition.inPieceDistance);
    console.log('piecesInside', _.map(piecesInside, function(p) {return p.index}))
    console.log('piecesInside', _.map(piecesInside, function(p) {return p.targetSpeed}))
    var minTargetPiece = _.min(piecesInside, function(piece) {
        return piece.targetSpeed;
    });
    console.log(minTargetPiece)

    if (speed > minTargetPiece.targetSpeed) {
        newThrottle = 0;
    }

    return newThrottle;
};



// Sets the target speed for piece
// pieceIndex: Pieces which targetspeed is to be set
// targetSpeed: Target speed for that piece
ThrottleControl.prototype.setTargetSpeed = function (pieceIndex, targetSpeed) {
};


ThrottleControl.prototype.printGameInfo = function () {
    var piece = null;

    for (var i = 0; i < this._pieces.length; ++i)
    {
        piece = this._pieces[i];

        console.log("--Throttle Control Race Data");
        console.log("Piece:", piece.pieceIndex);
        console.log("Target speed:", piece.targetSpeed);
        console.log("Braking distance:", piece.breakingDistance);
        console.log("Bend:", piece.isBend ? "true" : "false");
        console.log("--------------------------------------------");
    }
};
module.exports = ThrottleControl;

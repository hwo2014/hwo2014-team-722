var net = require("net");
var JSONStream = require('JSONStream');

var _ = require('lodash');
var utils = require('./utils');
var Bot = require('./last-and-furious');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
var carCount = process.argv[6];
var trackName = process.argv[7];
var password = process.argv[8];

var joinData = {
    botId: {
        name: botName,
        key: botKey
    },
    carCount: parseInt(carCount, 10)
};

if (trackName) {
    joinData.trackName = trackName;
}

if (password) {
    joinData.password = password;
}

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {

    if (carCount) {
        console.log('joinRace')
        console.log(joinData)
        return send({msgType: "joinRace", data: joinData });
    }

    console.log('join normal')
    return send({
        msgType: "join",
        data: {
            name: botName,
            key: botKey
        }
    });
});

function send(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
}

function ping() {
    return send({msgType: "ping", data: {}});
}

jsonStream = client.pipe(JSONStream.parse());

// Initialize our bot
var bot = new Bot();

jsonStream.on('data', function(protocolData) {
    var messages = {
        'join': bot.onJoin,
        'gameStart': bot.onGameStart,
        'gameInit': bot.onGameInit,
        'carPositions': bot.onCarPositions,
        'gameEnd': bot.onGameEnd,
        'yourCar': bot.onYourCar,
        'tournamentEnd': bot.onTournamentEnd,
        'crash': bot.onCrash,
        'spawn': bot.onSpawn,
        'turboAvailable': bot.onTurboAvailable
    };

    if (protocolData.msgType === 'error') {
        console.error(protocolData.data);
    }

    if (_.has(messages, protocolData.msgType)) {
        console.log('\n-- tick', protocolData.gameTick);

        // Find corresponding function for msgType
        var response = messages[protocolData.msgType].apply(
            bot, [protocolData.data, protocolData.gameTick]
        );

        if (!response) {
            ping();
        } else {
            send(response);
        }

    } else {
        // If server sends crap, response ping
        ping();
    }
});

jsonStream.on('error', function() {
    return console.log("disconnected");
});

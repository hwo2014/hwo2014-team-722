var _ = require('lodash');

var RingBuffer = require('./ringbuffer');
var utils = require('./utils');
var ThrottleControl = require('./throttle-control');
var SpeedPredict = require('./prediction');


var Bot = function Bot(opts) {
    var defaultOpts = {
    };

    this.opts = _.extend(defaultOpts, opts);
    this._previousCarPositions = new RingBuffer(5);
    this._turboAvailable = false;
    this._piecesDriven = 0;
};

Bot.prototype.onJoin = function(data) {
    console.log('onJoin');

};

Bot.prototype.onGameStart = function(data) {
    console.log('onGameStart');

};

Bot.prototype.onGameInit = function(data) {
    console.log('onGameInit');
    console.log('pieces', data.race.track.pieces);
    this._game = data;
    this._pieces = this._game.race.track.pieces;
    this._lanes = this._game.race.track.lanes;

    this._speedPredict = new SpeedPredict(this, this._game);
    this._throttleControl = new ThrottleControl(this._game, this._speedPredict, this._pieces);
    this._lastThrottle = 0;

    if (_.has(this._game.race.raceSession, 'laps')) {
        this._lastLapIndex = this._game.race.raceSession.laps - 1;
    } else {
        this._lastLapIndex = 2;
    }

    var pieces = this._game.race.track.pieces;
    var lanes = this._game.race.track.lanes;
    var laneLengthsBetween = utils.laneLengthsBetweenSwitches(lanes, pieces);

    var switchPieceIndexes = utils.switchPieceIndexes(pieces);
    for (var i = 0; i < switchPieceIndexes.length; ++i) {
        var switchIndex = switchPieceIndexes[i];
        var lengths = laneLengthsBetween[i];

        this._game.race.track.pieces[switchIndex].lengthsToNextSwitch = lengths;
    }

    for (var i = 0; i < pieces.length; ++i) {
        var piece = pieces[i];
        if (utils.isBend(piece)) {
            piece.length = utils.bendLength(0, piece.angle, piece.radius);
        }
        piece.index = i;
    }

    this._resetHasSwitched();
};

Bot.prototype.onCarPositions = function(data, tick) {

    // First car positions
    if (_.isUndefined(tick)) {
        console.log('Initial car positions');
    }

    this._previousCarPositions.push(data);
    this._checkIfCarHasSwitchedLap(data);
    this._speedPredict.newCarPositions(data);

    var pieces = this._game.race.track.pieces;
    var lanes = this._game.race.track.lanes;

    var myCarPos = this.getColorPosition(data, this._myColor);
    console.log(myCarPos);

    var speed = this.getSpeed(this._myColor, 0);
    console.log('Speed:', speed);

    var accleration = this._getMyAccleration();
    console.log('accleration:', accleration);

    if (speed > 10) {
        console.log('-- ALERT: speed', speed);
    }

    var myPieceIndex = myCarPos.piecePosition.pieceIndex;
    var myCurrentPiece = pieces[myPieceIndex];

    var myPiece1Ahead = utils.nextPiece(pieces, myPieceIndex, 1);
    var myPiece2Ahead = utils.nextPiece(pieces, myPieceIndex, 2);
    var myPiece3Ahead = utils.nextPiece(pieces, myPieceIndex, 3);
    var myPiece4Ahead = utils.nextPiece(pieces, myPieceIndex, 4);

    var throttle = this._throttleControl.getThrottle(myCarPos.piecePosition, speed, accleration, this._lastThrottle);
    this._lastThrottle = throttle;
    console.log('throttle', throttle);

    var targetSpeed = this._speedPredict.getTargetSpeed(myPieceIndex);
    console.log('myPieceIndex', myPieceIndex, 'target speed:', targetSpeed)
    utils.plotToCsv(tick, speed, targetSpeed, accleration, throttle, myCarPos.angle);

    var currentLap = myCarPos.piecePosition.lap;
    if (this._turboAvailable && currentLap === this._lastLapIndex) {

        var straightPieces = [];
        var i = 0;
        while (true) {
            var piece = utils.nextPiece(pieces, myPieceIndex, i);
            if (utils.isBend(piece)) {
                break;
            } else {
                straightPieces.push(piece);
            }

            ++i;
        }
        for (var i = 0; i < straightPieces.length; ++i) {
            var piece = straightPieces[i];
            if (piece.index === pieces.length - 1) {
                this._turboAvailable = false;
                return {
                    msgType: 'turbo',
                    data: 'gotta go fast'
                };
            }
        }
    }

    var decision = this._getSwitchDecision(myCarPos, myPiece1Ahead);
    if (decision !== false) {
        return decision;
    }

    return {
        msgType: 'throttle',
        data: throttle
    };

};

Bot.prototype.onCarLapSwitch = function(color) {
    if (color === this._myColor) {
        console.log('Our car switched lap, reset switched info');
        this._resetHasSwitched();
    }
};

Bot.prototype.onTurboAvailable = function(data) {
    console.log('onTurboAvailable');
    this._turboAvailable = true;
};

Bot.prototype.onGameEnd = function(data) {
    console.log('onGameEnd');

};

Bot.prototype.onCrash = function(data) {
    console.log('onCrash');
    console.log(data);
    this._speedPredict.onCrash(data);
};

Bot.prototype.onSpawn = function(data) {
    console.log('onSpawn');
    console.log(data);
    this._speedPredict.onSpawn(data);

};

Bot.prototype.onYourCar = function(data) {
    console.log('onYourCar');
    this._myColor = data.color;
};

Bot.prototype.onTournamentEnd = function(data) {
    console.log('onTournamentEnd');
};

Bot.prototype._resetHasSwitched = function() {
    _.each(this._game.race.track.pieces, function(piece) {
        if (utils.isSwitch(piece)) {
            piece.isSwitchDecisionDone = false;
        }
    });
};

Bot.prototype._checkIfCarHasSwitchedLap = function(currentCarPositions) {
    // Check if a car has switched a lap
    var previousPositions = this._previousCarPositions.newest(1);

    if (_.isUndefined(previousPositions)) {
        return;
    }

    for (var i = 0; i < previousPositions.length; ++i) {
        var color = previousPositions[i].id.color;
        var currentCarPosition = this.getColorPosition(currentCarPositions, color);

        // This code assumes that it can. If no, this checking can be removed.
        if (currentCarPosition) {

            var previousIndex = previousPositions[i].piecePosition.pieceIndex;
            var currentIndex = currentCarPosition.piecePosition.pieceIndex;

            if (currentIndex !== previousIndex && color === this._myColor) {
                this._piecesDriven += 1;
                if (this._piecesDriven === this._pieces.length) {
                    this._speedPredict.onMyCarLapSwitch();
                }
            }

            var previousCarPosition = previousPositions[i];
            if (currentIndex < previousIndex) {
                this.onCarLapSwitch(color);
            }
        }
    }
};


Bot.prototype._getSwitchDecision = function(myCarPosition, myPiece1Ahead) {
    if (utils.isSwitch(myPiece1Ahead) && !myPiece1Ahead.isSwitchDecisionDone) {
        var shortestLaneData = utils.shortestLane(myPiece1Ahead.lengthsToNextSwitch);

        // If we have equal route lengths between switches, keep current lane
        // by default
        var lengths = _.pluck(myPiece1Ahead.lengthsToNextSwitch, 'length');

        console.log('Switch decision -------------');
        console.log(lengths);
        console.log('\n\n');

        var areAllLengthsSame = utils.allNumbersEqual(lengths);
        console.log('All are same', areAllLengthsSame);

        var myCurrentLaneIndex = myCarPosition.piecePosition.lane.startLaneIndex;
        if (myCurrentLaneIndex != shortestLaneData.index && !areAllLengthsSame) {

            var direction = 'Left';
            if (myCurrentLaneIndex < shortestLaneData.index) {
                direction = 'Right';
            }

            if (myCurrentLaneIndex % 2 === 0) {
                direction = 'Left';
            }

            console.log('Switch to direction:', direction);
            myPiece1Ahead.isSwitchDecisionDone = true;

            return {
                msgType: 'switchLane',
                data: direction
            };
        } else {
            console.log('Not going to switch');
            myPiece1Ahead.isSwitchDecisionDone = true;
        }
    }

    return false;
};

Bot.prototype.getSpeed = function(color, newest) {
    var pieces = this._game.race.track.pieces;
    var lanes = this._game.race.track.lanes;

    var currentPositions = this._previousCarPositions.newest(newest);
    var previousPositions = this._previousCarPositions.newest(newest + 1);

    var currentCarPos = this.getColorPosition(currentPositions, color);
    var previousCarPos = this.getColorPosition(previousPositions, color);
    var speed = utils.speed(lanes, pieces, previousCarPos, currentCarPos);
    if (speed === false) {
        speed = this.getSpeed(color, newest + 1);
    }

    return speed;
};

Bot.prototype._getMyAccleration = function() {
    if (this._previousCarPositions.size() < 2) {
        return this.getSpeed(this._myColor, 0);
    }

    var previousSpeed = this.getSpeed(this._myColor, 1);
    var currentSpeed = this.getSpeed(this._myColor, 0);
    return currentSpeed - previousSpeed;
};

Bot.prototype.getColorPosition = function(carPositions, color) {
    if (!_.isArray(carPositions)) {
        return undefined;
    }

    for (var i = 0; i < carPositions.length; ++i) {
        if (carPositions[i].id.color === color) {
            return carPositions[i];
        }
    }
};

module.exports = Bot;

// General game functions

var fs = require('fs');
var _ = require('lodash');



function cycleIndex(arr, i, addition) {
    return (i + addition) % arr.length;
}

function cycle(arr, index) {
    return arr[index % arr.length];
}

function speed(lanes, pieces, previousCarPosition, currentCarPosition) {

    var currentPiecePos = currentCarPosition.piecePosition;

    if (!previousCarPosition) {
        return currentPiecePos.inPieceDistance;
    }

    var currentPiece = pieces[currentPiecePos.pieceIndex];
    var previousPiecePos = previousCarPosition.piecePosition;

    if (previousPiecePos.pieceIndex === currentPiecePos.pieceIndex) {
        var previousInDistance = previousPiecePos.inPieceDistance;
        return calculateDistance(previousInDistance, currentPiecePos);
    }

    return false;
}

function piecesInsideLength(pieces, length, index, inPieceDistance) {
    var lengthUsed = pieces[index].length - inPieceDistance;

    var i = index + 1;
    var piecesUsed = [cycle(pieces, i)];
    while (true) {
        var piece = cycle(pieces, i);
        piecesUsed.push(piece);
        lengthUsed += piece.length;

        if (lengthUsed > length) {
            return piecesUsed;
        }

        ++i;
    }
}

function calculateDistance(previousInDistance, currentPiecePos) {
    return currentPiecePos.inPieceDistance - previousInDistance;
}

function laneLengthsBetweenSwitches(lanes, pieces) {
    var switchIndexes = switchPieceIndexes(pieces);

    var laneLengthsBetween = [];
    for (var i = 0; i < switchIndexes.length; ++i) {
        var nextIndex = cycleIndex(switchIndexes, i, 1);
        var piecesBetween = piecesBetweenIndexes(
            pieces,
            switchIndexes[i],
            switchIndexes[nextIndex]
        );

        var lengths = laneLengths(lanes, piecesBetween);
        laneLengthsBetween.push(lengths);
    }

    return laneLengthsBetween;
}

function piecesBetweenIndexes(pieces, startIndex, endIndex) {
    var piecesBetween = [];

    var index = cycleIndex(pieces, startIndex, 1);
    while (index != endIndex) {
        var piece = pieces[index];

        piecesBetween.push(piece);
        index = cycleIndex(pieces, index, 1);
    }

    return piecesBetween;
}

function switchPieceIndexes(pieces) {
    var indexes = [];

    for (var i = 0; i < pieces.length; ++i) {
        if (isSwitch(pieces[i])) {
            indexes.push(i);
        }
    }

    return indexes;
}

function plotToCsv(tick, speed, targetSpeed, accleration, throttle, angle) {
    if (!tick) {
        return;
    }

    if (targetSpeed > 20) {
        targetSpeed = 20;
    }

    var data = [tick, speed, targetSpeed, accleration, throttle, angle];
    var row = data.join(',')
    fs.appendFile('plotter/telemetry.csv', row + '\n', function (err) {
        if(err) {
            console.log(err);
        } else {
            console.log("Telemetry saved");
        }
    });
}

// Returns next piece relative to currentIndex
function nextPiece(pieces, currentIndex, addition) {
    return cycle(pieces, currentIndex + addition);
}

function isBend(piece) {
    return _.has(piece, 'angle');
}

function isSwitch(piece) {
    return _.has(piece, 'switch');
}

// Returns position after `tickCount` ticks
function positionAfterTicks(game, tickCount) {
    var position;
    // Calc stuff
    return position;
}

function shortestLane(laneDatas) {
    return _.min(laneDatas, function(laneData) {
        return laneData.length;
    });
}

// Returns car positions
function inPiecePositions(inPieceDistance, carDimensions) {
    var front = inPieceDistance + carDimensions.guideFlagPosition;
    return {
        front: front,
        rear: front - carDimensions.length
    };
}

function laneLengths(lanes, pieces) {
    var laneDatas = _.map(lanes, function(lane) {
        return {length: 0, index: lane.index};
    });

    _.each(pieces, function(piece) {
        if (isBend(piece)) {
            // Find length for each lane in bend
            _.each(bendLaneLengths(lanes, piece.angle, piece.radius), function(laneData) {
                laneDatas[laneData.index].length += laneData.length;
            });
        } else {
            // In straight piece, all lengths are same
            for (var i = 0; i < lanes.length; ++i) {
                laneDatas[i].length += piece.length;
            }
        }

    });

    return laneDatas;
}

function allNumbersEqual(arr) {
    var first = arr[0];
    for (var i = 0; i < arr.length; ++i) {
        if (Math.abs(arr[i] - first) > 0.0001) {
            return false;
        }
    }

    return true;
}

// [
//  {length: 10, index: 0}
// ]
function bendLaneLengths(lanes, angle, radius) {
    return _.map(lanes, function(lane) {

        var laneData = {
            length: bendLength(lane.distanceFromCenter, angle, radius),
            index: lane.index,
            radius: calculateRadius(lane.distanceFromCenter, angle, radius)
        };

        return laneData;
    });
}

function bendLength(distanceFromCenter, angle, radius) {
    var realRadius = calculateRadius(distanceFromCenter, angle, radius);
    return (Math.abs(angle) / 360) * 2 * Math.PI * realRadius;
}

function calculateRadius(distanceFromCenter, angle, radius) {
    var sign = angle / Math.abs(angle);
    return radius - (sign * distanceFromCenter);
}

function calculateAngleSpeed(radius, speed)
{
    var angleSpeed = speed / (2 * Math.PI * radius) * 360;
    return angleSpeed;
}

function calculateSpeed(radius, angleSpeed)
{
    var speed = (2 * Math.PI * radius * angleSpeed) / 360;
    return speed;
}

function firstBendIndex(pieces) {
    for (var i = 0; i < pieces.length; ++i) {
        if (isBend(pieces[i])) {
            return i;
        }
    }
}


module.exports = {
    cycle: cycle,
    nextPiece: nextPiece,
    isBend: isBend,
    positionAfterTicks: positionAfterTicks,
    shortestLane: shortestLane,
    inPiecePositions: inPiecePositions,
    laneLengths: laneLengths,
    bendLaneLengths: bendLaneLengths,
    bendLength: bendLength,
    laneLengthsBetweenSwitches: laneLengthsBetweenSwitches,
    switchPieceIndexes: switchPieceIndexes,
    isSwitch: isSwitch,
    allNumbersEqual: allNumbersEqual,
    speed: speed,
    plotToCsv: plotToCsv,
    calculateAngleSpeed: calculateAngleSpeed,
    calculateSpeed: calculateSpeed,
    firstBendIndex: firstBendIndex,
    piecesInsideLength: piecesInsideLength
};

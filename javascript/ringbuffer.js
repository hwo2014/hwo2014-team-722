var RingBuffer = function(size) {
    this._size = size;
    this._array = [];
};

RingBuffer.prototype.push = function(item) {
    if (this._array.length >= this._size) {
        this._array.shift();
    }

    this._array.push(item);
};

RingBuffer.prototype.size = function() {
    return this._array.length;
};

// newest(0)-> newest element, newest(1) -> second newest etc..
RingBuffer.prototype.newest = function(index) {
    if (this.size() <= 0) {
        return undefined;
    }

    return this._array[(this._array.length - 1) - index];
};


module.exports = RingBuffer;

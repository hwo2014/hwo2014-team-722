var _ = require('lodash');

var utils = require('./utils');
var RingBuffer = require('./ringbuffer');


var SpeedPredict = function SpeedPredict(bot, game) {
    this._bot = bot;
    this._game = game;
    this._pieces = this._game.race.track.pieces;
    this._lanes = this._game.race.track.lanes;
    this._isActualRace = _.has(this._game.raceSession, 'quickRace');
    this._initialPrediction(this._pieces, this._lanes);

    this._firstBendIndex = utils.firstBendIndex(this._pieces);
    this._maxAngle = 60;
    this._maxAngleSpeed = 0;
    this._maxAngleSpeedRadius = 10;
    this._speedLearnRatio = 0.55;
    this._crashSeen = false;
    this._isFirstLap = true;
    this._maxAngleThreshold = 0.5;
    this._ourCarOut = false;

    this._previousCarPositions = new RingBuffer(30);
};


SpeedPredict.prototype.onMyCarLapSwitch = function() {
    console.log('prediction lap switch')
    _.each(this._pieces, function(piece) {
        if (utils.isBend(piece)) {
            if (piece.bestSeenSpeed > 1) {
                piece.targetSpeed = piece.bestSeenSpeed;
                console.log('set piece to new target speed', piece.targetSpeed);
            }
        }
    });

    this._isFirstLap = false;
    console.log('not first lap anymore')
};

SpeedPredict.prototype._initialPrediction = function(pieces, lanes) {
    var targetSpeeds = {};

    _.each(pieces, function(piece) {
        if (utils.isBend(piece)) {
            var bendLaneLengths = utils.bendLaneLengths(lanes, piece.angle, piece.radius);
            var shortestLaneData = utils.shortestLane(bendLaneLengths);
            console.log('shortestradius', shortestLaneData.radius)
            // Our best prediction at the start is: speed 6 for 80 radius bends
            piece.targetSpeed = Infinity;
            piece.maxSeenSpeed = 0;
            piece.bestSeenSpeed = 0;
        } else {
            piece.targetSpeed = Infinity;
        }
    });
};

// newCarPositions can be fed constantly and the prediction improves
SpeedPredict.prototype.newCarPositions = function(carPositions) {
    this._previousCarPositions.push(carPositions);

    var self = this;
    _.each(carPositions, function(carPos) {
        self._analyzeCarPos(carPos);
    });
};

SpeedPredict.prototype.getTargetSpeed = function(pieceIndex) {
    return this._pieces[pieceIndex].targetSpeed;
};

SpeedPredict.prototype.onCrash = function(data) {
    if (this._isFirstLap && !this._crashSeen) {
        this._calculateTargetSpeeds();
    }

    this._crashSeen = true;

    if (data.color === this._bot._myColor) {
        this._ourCarOut = true;
    }
};

SpeedPredict.prototype.onSpawn = function(data) {
    if (data.color === this._bot._myColor) {
        this._ourCarOut = false;
    }
};

SpeedPredict.prototype._analyzeCarPos = function(carPos) {

    if (this._previousCarPositions.size() < 2) {
        return;
    }

    console.log('Analyze car pos')
    var speed = this._bot.getSpeed(carPos.id.color, 0);
    var angle = Math.abs(carPos.angle);
    var previousAngle = this._getPreviousAngle(carPos.id.color);
    var angleDelta = Math.abs(angle - Math.abs(previousAngle));

    var angleDiff = Math.abs(angle) - this._maxAngle * this._maxAngleThreshold;

    var pieceIndex = carPos.piecePosition.pieceIndex;
    var currentPiece = this._pieces[pieceIndex];
    var currentLap = carPos.piecePosition.lap;

    if (utils.isBend(currentPiece)) {
        console.log('is bend')
        if (speed > currentPiece.maxSeenSpeed) {
            currentPiece.maxSeenSpeed = speed;
            console.log('New max seen speed:', speed);
        }

        if (angle > this._maxAngle) {
            this._maxAngle = Math.abs(carPos.angle);
            console.log('New max angle:', carPos.angle);
        }

        if (angle < this._maxAngle * 0.8 && speed > currentPiece.bestSeenSpeed) {
            currentPiece.bestSeenSpeed = speed;
            console.log('New best seen speed:', speed);
        }

        var angleSpeed = this._getAngleSpeed(carPos.id.color);
        console.log('Angle speed:', angleSpeed);
        if (!_.isUndefined(angleSpeed) && angleSpeed > this._maxAngleSpeed) {
            console.log('New max angle speed:', angleSpeed);
            this._maxAngleSpeed = angleSpeed;
            this._maxAngleSpeedRadius = currentPiece.radius;
        }

    }

    if (carPos.id.color !== this._bot._myColor || this._ourCarOut) {
        return;
    }

    if (this._crashSeen) {
        if (speed > 0 && angleDelta > 3.5 && utils.isBend(currentPiece)) {
            console.log('angle delta mor than 6');
            currentPiece.targetSpeed = speed * 0.7;
        }

        console.log('crash has been seen, adjust speed')
        if (!this._isFirstLap) {
            console.log('not first lap anymore, return')
            return;
        }

        if (utils.isBend(currentPiece)) {
            console.log('angleDiff', angleDiff)

            if (speed > 0 && angleDiff > 0) {
                console.log('Slow down!')
                currentPiece.targetSpeed = speed * 0.7;
                console.log('Set target speed to', currentPiece.targetSpeed)
            }
            if (Math.abs(speed - currentPiece.targetSpeed) < 0.2 && angleDiff < -15) {
                console.log('More speed!')
                currentPiece.targetSpeed = currentPiece.targetSpeed * 1.1;
                console.log('Set target speed to', currentPiece.targetSpeed)
            }
        } else {
            // Straight
            if (angleDiff > 0) {
                console.log('This is straight piece, Slow down!')
                currentPiece.targetSpeed = 0;
            } else {
                currentPiece.targetSpeed = Infinity;
            }
        }
    } else {
        console.log('no crash seen, THROTTLE');
    }
};

SpeedPredict.prototype._getPreviousAngle = function(color) {
    var previousPositions = this._previousCarPositions.newest(1)
    var previousCarPos = this._bot.getColorPosition(previousPositions, color);
    return previousCarPos.angle;
};

SpeedPredict.prototype._getAngleSpeed = function(color) {
    var currentPositions = this._previousCarPositions.newest(0);
    var previousPositions = this._previousCarPositions.newest(1);

    var currentCarPos = this._bot.getColorPosition(currentPositions, color);
    var previousCarPos = this._bot.getColorPosition(previousPositions, color);
    var currentPiece = this._pieces[currentCarPos.piecePosition.pieceIndex];
    var previousPiece = this._pieces[previousCarPos.piecePosition.pieceIndex];

    if (utils.isBend(previousPiece) && utils.isBend(currentPiece) && currentCarPos.pieceIndex === previousCarPos.pieceIndex) {
        var speed = utils.speed(this._lanes, this._pieces, previousCarPos, currentCarPos);
        console.log('speed', speed)
        console.log(currentPiece.radius)
        console.log(utils.calculateAngleSpeed(currentPiece.radius, speed))
        return utils.calculateAngleSpeed(currentPiece.radius, speed);
    }

    return undefined;
};

SpeedPredict.prototype._calculateTargetSpeeds = function() {
    console.log('calc target speeds');
    var self = this;

    _.each(this._pieces, function(piece) {
        if (utils.isBend(piece)) {
            console.log(piece.radius, self._maxAngleSpeed);
            var speed = utils.calculateSpeed(piece.radius, self._maxAngleSpeed);

            console.log('pieceIndex', piece.index,' new calculated target speed ', speed);
            piece.targetSpeed = speed * self._speedLearnRatio;
            if (piece.radius < this._maxAngleSpeedRadius) {
                piece.targetSpeed = piece.targetSpeed * (piece.radius / this._maxAngleSpeedRadius);
            }

        } else {
            piece.targetSpeed = Infinity;
        }
    });
};


module.exports = SpeedPredict;
